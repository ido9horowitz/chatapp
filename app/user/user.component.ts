import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { User } from '../user/user';



@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})

export class UserComponent implements OnInit {

user: User;
@Output() deleteEvent = new EventEmitter();
@Output() editEvent = new EventEmitter();

text:String = '';
isEdit:Boolean = false;

editButtonText = "Edit";

showPostButton = "Show Posts";

postOn:Boolean = false;

  constructor() { }

  sendDelete(){
    this.deleteEvent.emit(this.user);
  }

  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText ="Save" : this.editButtonText = "Edit";
    if(!this.isEdit){
      this.editEvent.emit(this.user);
      console.log(this.user);
    }
  }
  showPosts(){

     this.postOn = !this.postOn;
     this.postOn ? this.showPostButton ="Hide Posts" : this.showPostButton = "Show Posts";
     console.log(this.postOn);
     return this.postOn;
  }

  ngOnInit() {
  }

}
