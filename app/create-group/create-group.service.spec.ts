/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CreateGroupService } from './create-group.service';

describe('Service: CreateGroup', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateGroupService]
    });
  });

  it('should ...', inject([CreateGroupService], (service: CreateGroupService) => {
    expect(service).toBeTruthy();
  }));
});
