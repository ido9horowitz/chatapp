import { Injectable } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';


@Injectable()
export class CreateGroupService {

groupObservable;


createGroup(group, groupID){

   this.af.database.list('/groups/'+groupID).push(group);
   const toSend = this.af.database.object(`/groups/${groupID}`);
   toSend.set(group);
}

getDetailsForGroup(groupKey){
  this.groupObservable = this.af.database.list('/groups/'+ groupKey);
  return this.groupObservable;
}

getLastGroup(){
  this.groupObservable = this.af.database.list(`/groups/`, {
    query: {
        limitToLast:1
    }
  });
  return this.groupObservable;
}

 getGroupsJoin(authEmail){
    this.groupObservable = this.af.database.list('/users').map(
      users =>{
        users.map(
          user => {

            user.userGroups = [];
            for(var g in user.groupIn){
                 if(user.email == authEmail){
                      //console.log(user);
                      user.userGroups.push(
                      this.af.database.object('/groups/'  +g)
                    )
                  }
                }
           }
        );
        //console.log(users);
        return users;

      }
    )

    
    //this.usersObservable = this.af.database.list('/users');     
     return this.groupObservable;
 	}

  constructor(private af:AngularFire) { }

}
