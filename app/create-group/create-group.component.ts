import { Component, trigger, transition, style, animate ,OnInit, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { UsersService } from '../users/users.service';
import { CreateGroupService } from './create-group.service';
import { User } from '../user/user';
import { Router } from '@angular/router';
import { AuthService } from '../providers/auth.service';

@Component({
  selector: 'app-create-group',
  host: {'(document:click)': 'handleClick($event)',},
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(100%)', opacity: 0}),
          animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('500ms', style({transform: 'translateX(100%)', opacity: 0 }))
        ])
      ]
    )
  ],
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
  
})
export class CreateGroupComponent implements OnInit {
isLoading:boolean = true;

public filteredList = [];
public findCurrent = [];
public addUser;
public query = '';
public groupName = '';
public wasInvited: Boolean = false;
public showGroup: Boolean = false;
public showGroupDetails:Boolean = false;
public elementRef;
public usersArray:Array<User> = [];
public myData:any;
public i:number;
public usersObjArray:Array<Object> = [];
public invitedArray:Array<String> = [];
public emptyGroup:Boolean = false;
public noGroupName:Boolean = false;
public currentAuthEmail;
public currentAuthFullName:String;
public currentAuthKey:String;
public groupID:number;
public currentAuth= {};
public myGroupsData =[];
public tmpGroupsData =[];
public myGroupsMembers =[];

public users;



  constructor(private authService: AuthService, myElement: ElementRef, private usersService: UsersService, private createGroupService: CreateGroupService, private router: Router) {
      this.elementRef = myElement;
       
    }
    
  ngOnInit() {

//get group getGroupDetails
 ///find current auth name
    this.usersService.getUsers().subscribe((data)=>{
        let n = 0;
        while(n < data.length){
            if(data[n].email == this.currentAuthEmail){
                this.currentAuth = data[n];
                 this.invitedArray['groupLeader'] = this.currentAuth;
    
                this.currentAuthFullName = data[n].firstname + ' ' + data[n].lastname;     
                this.currentAuthKey = data[n].$key;
            }
            n = n+1;
        }
        //console.log(this.currentAuth);
    });
    

      this.usersService.getUsers().subscribe((data)=>{
      this.usersArray = data;
    });

   
        this.usersService.angularFire.auth.subscribe((data)=>{
        
        this.currentAuthEmail = data.auth.email;
        //console.log(this.currentAuthEmail);
        
        /////users groups
            this.createGroupService.getGroupsJoin(this.currentAuthEmail).subscribe(usersData =>
            {
                
                this.users = usersData;
                this.users.forEach(user => {
                    //console.log(user);
                    user.userGroups.forEach(group=>{
                        group.subscribe(groupData=>{
                            //console.log(groupData);
                            this.myGroupsData.push(groupData);
                            this.isLoading = false;
                        });
                    });
                });
                console.log(this.myGroupsData);
            });


        }); 
   
  
    this.createGroupService.getLastGroup().subscribe(data=>{
        //console.log(data[0].$key);
        this.groupID = +data[0].$key;
        this.groupID ++;
       // console.log(this.groupID);
    });

    
    
    this.authService.af.auth.subscribe(data=>{
          if(data.auth.email == this.currentAuthEmail){
             // console.log(data.auth.email);
          }
      });
}


getGroupDetails(groupKey){
let count = 0;
    if(this.showGroupDetails){
        this.myGroupsMembers = [];
    }

        //console.log(groupKey);
        this.createGroupService.getDetailsForGroup(groupKey).subscribe(data=>{
            data.forEach(member => {
                if(member.$key != 'groupName'){
                count++;
                //console.log('member: '+ member);
                    this.myGroupsMembers.push(member);
                }
                //console.log(count);
            });
            ///count how many members and push to the next 
            this.myGroupsMembers[count] = {
                groupKey:''
            };
            this.myGroupsMembers[count].groupKey = (groupKey);
            
        });
    
    //console.log(this.myGroupsMembers);
    this.showGroupDetails = true;
    return this.myGroupsMembers;

}



createGroup(){
    let hasGroup = true;
    this.invitedArray['groupName'] = this.groupName;

    this.noGroupName = false;
    this.emptyGroup = false;
    

    //console.log(this.invitedArray.length);
    
    if(this.invitedArray['groupName'] == '' ){
        //validate group name
        this.noGroupName = true;
    }
    if(this.invitedArray.length == 0 ){
        //validate group members
        this.emptyGroup = true;
    }else{  
        this.invitedArray.forEach(user => {
            this.usersService.updateUser(user);
        });
        this.usersService.updateUser(this.invitedArray['groupLeader']);
        this.createGroupService.createGroup(this.invitedArray, this.groupID);
     
    }
}

showList(){
    this.showGroup = true;
}

containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i] === obj) {
            return true;
        }
    }

    return false;
}
 
  filter() {

    if (this.query !== ""){
        this.filteredList = this.usersArray.filter(function(el){
          if((   (el.email.includes(this.query.toLowerCase()))
              || (el.firstname.includes(this.query.toLowerCase()))
              || (el.lastname.includes(this.query.toLowerCase()))
              && (!this.usersObjArray.includes(el)) )){
            
            this.filteredList.push(el);
           
              return this.filteredList;
          }
        }.bind(this));
    }else{
        this.filteredList = [];
    }
    
  }
  
handleClick(event){
    
   var clickedComponent = event.target;
   var inside = false;
   do {
       if (clickedComponent === this.elementRef.nativeElement) {
           inside = true;
       }
      clickedComponent = clickedComponent.parentNode;
   } while (clickedComponent);
    if(!inside){
        this.filteredList = [];

    }
}
 
select(item){
  
      
    this.emptyGroup = false;
    this.query = '';

    //console.log(item);
    if( (this.currentAuthEmail != item.email)  && !this.containsObject(item, this.invitedArray)){
    
      this.invitedArray.push(item);

      this.invitedArray['groupLeader'].groupIn[this.groupID] = true;
      item.groupIn[this.groupID] = true;
     // console.log(this.invitedArray);
    }else{
        this.wasInvited = true;
       // this.invitedArray['wasInvited'] = '';
  }
    
    this.wasInvited = false;
    console.log(this.invitedArray);
    this.filteredList = [];


  }
  removeUser(user){
      let index = this.invitedArray.findIndex(x => x == user );
      this.invitedArray.splice(index, 1);
  }

}
