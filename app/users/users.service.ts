import { Injectable } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';



@Injectable()
export class UsersService {


usersObservable;
angularFire = this.af;

getUsers(){

  this.usersObservable = this.af.database.list('/users'); 
  return this.usersObservable;
}
addUser(user){
  this.af.database.list('/users').push(user);
}
updateUser(user){
  //  console.log(user.$key);
  let userkey = user.$key;
  let userData = { firstname:user.firstname, lastname:user.lastname, email:user.email, password:user.password, groupIn:user.groupIn};
  this.af.database.object('/users/'+ userkey).update(userData);
}
deleteUser(user){
  let userkey = user.$key;
  this.af.database.object('/users/'+ userkey).remove();
}
 getUsersJoin(){
    this.usersObservable = this.af.database.list('/users').map(
      users =>{
        users.map(
          user => {
            user.posTitles = [];
            for(var p in user.posts){
                user.posTitles.push(
                this.af.database.object('/posts/'  +p)
              )
            }
            if(user.posTitles.length == 0){ user.posTitles[0] = 0}
          }
        );
        return users;

      }
    )
    //this.usersObservable = this.af.database.list('/users');
     return this.usersObservable;
 	}



 
constructor(private af:AngularFire) {

}

}