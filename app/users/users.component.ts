import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles:[`
     li { cursor: pointer; }
     li:hover { background: #ecf0f1; }
     ul{
       list-style:none;
     }

  `]
})
export class UsersComponent implements OnInit {

isLoading:boolean = true;

hide:boolean = true;

users;

constructor(private _usersService: UsersService) { }

  ngOnInit() {

    console.log(this._usersService.angularFire.database);
    this._usersService.getUsersJoin().subscribe(usersData =>
    {
      this.users = usersData;
      console.log(this.users);
      this.isLoading = false;
    });
  }

  deleteUser(user){
    this._usersService.deleteUser(user);
  }

addUser(user){
  console.log(user);
  this._usersService.addUser(user);
}

/*
select(user){
  if(this.currentUser == -1){
  this.currentUser = user;
     console.log(user.id);
 
  }else{
    this.currentUser = -1;
  }

}*/
  updateUser(user){
    this._usersService.updateUser(user);
  }


}
