import { Injectable } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';


@Injectable()
export class ChatRoomService {

messageObservable;

getMessagesForGroup(groupKey){
  console.log()
      this.messageObservable = this.af.database.list('/messages/' + groupKey);
      return this.messageObservable;

}

addMessage(message, groupKey){
  this.af.database.list('/messages/' + groupKey).push(message);
}
  constructor(private af:AngularFire) { }

}
