import { Component, OnInit } from '@angular/core';
import { CreateGroupService } from '../create-group/create-group.service';
import { CreateGroupComponent } from '../create-group/create-group.component';
import { UsersService } from '../users/users.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../providers/auth.service';
import { ChatRoomService } from './chat-room.service';
@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css'],
  providers: [ CreateGroupComponent ]
})
export class ChatRoomComponent implements OnInit {
public currentAuthEmail;
public users;
public myGroupsData =[];
isLoading:boolean = true;
public isOnline:Boolean = false;
private sub: any;
public currentGroupKey;
public currentAuthKey;
public chatMembers = [];
public message:Object = {writer: '', body:' ', timestamp: '', groupKey: '', email: ''};
public currentAuthUser:Object;
public groupMessages = [];
public currentAuthPic: String;
  constructor(private chatRoomService:ChatRoomService, private authService:AuthService, private createGroupComponent: CreateGroupComponent , private createGroupService: CreateGroupService, private usersService:UsersService, private route: ActivatedRoute) { }

  ngOnInit() {

  this.usersService.angularFire.auth.subscribe((data)=>{
      this.currentAuthEmail = data.auth.email;


            let messagesDiv = document.getElementById("messagesDiv");
            messagesDiv.scrollTop = messagesDiv.scrollHeight;
            console.log(messagesDiv);

  }); 
        
  this.sub = this.route.params.subscribe(params=>{
        this.currentGroupKey = params['groupKey'];
        
        this.chatMembers = this.createGroupComponent.getGroupDetails(this.currentGroupKey);
         console.log(this.chatMembers);
        this.chatRoomService.getMessagesForGroup(this.currentGroupKey).subscribe(messages=>{
            this.groupMessages = messages;
    });
    


  });

}

    onKey(event: any){

    console.log(this.currentAuthEmail);
        let key = this.chatMembers[this.chatMembers.length-1].groupKey;
        let authorName;
        let body
        let email = this.currentAuthEmail;
        let time = Date.now();
        
        this.chatMembers.forEach(member => {
            if(member.email == this.currentAuthEmail){  
                authorName = member.firstname + ' ' + member.lastname;
            }
        });
        console.log(this.currentAuthUser);
        event.preventDefault();
        //console.log(event.keyCode);
        this.message = event.target.value;
        //console.log(this.message);
        body = event.target.value;
        if(event.keyCode == 13){
            this.message = {writer: authorName, body:body, timestamp: time, groupKey: key, email: email };    
            this.chatRoomService.addMessage(this.message, this.currentGroupKey);
            event.target.value = '';
        }
    }

}
