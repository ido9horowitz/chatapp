import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AuthService } from './providers/auth.service';
import { LoginPageComponent } from './login-page/login-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { CreateGroupComponent } from './create-group/create-group.component';

import { CreateGroupService } from './create-group/create-group.service';
import { ChatRoomComponent } from './chat-room/chat-room.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';

import { ChatRoomService } from './chat-room/chat-room.service';



export const fireConfig = {
    apiKey: "AIzaSyA_kkY-x1Mxo8YVy9dmUmUOm2-LwCkIYFk",
    authDomain: "chatapp-69137.firebaseapp.com",
    databaseURL: "https://chatapp-69137.firebaseio.com",
    storageBucket: "chatapp-69137.appspot.com",
    messagingSenderId: "620182195186"
}

const routes: Routes = [
  {path:'login', component: LoginPageComponent},
  {path:'chat/:groupKey', component: ChatRoomComponent},
  {path:'', component:HomePageComponent},
]


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    HomePageComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    CreateGroupComponent,
    ChatRoomComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(fireConfig),
    RouterModule.forRoot(routes)
  ],
  providers: [ AuthService, UsersService, CreateGroupService, ChatRoomService],
  bootstrap: [AppComponent]
})
export class AppModule { }
