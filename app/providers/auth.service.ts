import { Injectable } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
@Injectable()
export class AuthService {

  constructor(public af:AngularFire) { }

    createUser(email: string, password: string) {
      this.af.auth.createUser({ email: email, password: password });
    }

    
  loginWithEmail(email, password) {
    return this.af.auth.login({
        email: email,
        password: password,
      },
      {
        provider: AuthProviders.Password,
        method: AuthMethods.Password,
      });
  }
  
  loginWithGoogle() {
    return this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup,
    });
  }
   login(){
    return this.af.auth.login({
    provider: AuthProviders.Password,
    method: AuthMethods.Password
    });
   }

  logout(){
    return this.af.auth.logout();
  }

}
