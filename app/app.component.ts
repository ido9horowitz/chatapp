import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './providers/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
private isLoggedIn:Boolean =false;
private googleLog:Boolean = false;
private emailLog:Boolean = false;

private user_displayName:String;
private user_email:String;

  logout(){
     this.isLoggedIn = false;
     this.googleLog = false;
     this.emailLog = false;
    this.authService.af.auth.logout();
    this.router.navigate(['login']);
  }


  constructor( public authService:AuthService, private router:Router){
    this.authService.af.auth.subscribe(
      (auth) => {
        if(auth == null){
          //not logged in
          this.isLoggedIn = false;
          this.router.navigate(['login']);
        }else{
          //logged in
             this.isLoggedIn = true;

            if(auth.google){
              this.googleLog = true;
              this.user_displayName = auth.google.displayName;
              this.user_email = auth.google.email;
              this.router.navigate(['']);
            }else{
        
            this.emailLog = true;
            this.user_displayName = auth.auth.displayName;
            this.user_email = auth.auth.email;
            //this.router.navigate(['']);
          }
        }
      }
    );
  }

}
