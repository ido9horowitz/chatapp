import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../providers/auth.service';
import { Router } from '@angular/router';
import { UsersService } from '../users/users.service';
import { User } from '../user/user';
import { NgForm } from '@angular/forms';
import { UsersComponent } from '../users/users.component';



@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
  providers:[ UsersComponent]
})
export class LoginPageComponent implements OnInit {


user:User = {firstname:'',lastname:'',  email:'', password:'', groupIn:{a:''} 
};
public error: any;
public register:Boolean = false;



  constructor( private usersComponent:UsersComponent, private authService: AuthService, private router: Router, private _usersService: UsersService) { }



  ngOnInit() {

  }


 loginWithEmail(event, email, password){
    event.preventDefault();
    this.authService.loginWithEmail(email, password).then((data) => {
    this.router.navigate(['']);
    })
      .catch((error: any) => {
        if (error) {
          this.error = error;
          console.log(this.error);
        }
      });
  }
  
  loginWithGoogle() {
    this.authService.loginWithGoogle().then((data) => {
      // Send them to the homepage if they are logged in
      if(data.auth.displayName){
            
        var str = data.auth.displayName;
        var fname = str.split(" ")[0];
        var lname = str.split(" ")[1];

        this.user.firstname = fname;
        this.user.lastname = lname;
      }
      if(data.auth.email){
        this.user.email = data.auth.email;
        this.router.navigate(['']);
        
      }else{
         this.user.email = data.auth.email;
         this.router.navigate(['']);

      }

    })
  }

   onSign(form:NgForm){
     //Sign in a new user
    this.user.email = form.form.value.email;
    this.user.password = form.form.value.password;
    this.user.firstname = form.form.value.firstname;
    this.user.lastname = form.form.value.lastname;

    
    console.log(this.user);
    this.authService.createUser(this.user.email, this.user.password);
      if(this.authService.af.auth){
        //this.loginWithEmail(event, this.user.email, this.user.password);
        this.usersComponent.addUser(this.user);
        this.router.navigate(['']);
        //this.register = false;
      }
    };
  


  showRegister(){

    if(this.showRegister){
      this.register = !this.register;
    }
  }



  }
  




  /*ogin(){
    this.authService.loginWithGoogle().then((data) =>{
      
      this.router.navigate(['']);

    })
  }
  */
